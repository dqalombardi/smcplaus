import pytest

from smcplaus.exceptions import IllDefinedStructureError
from smcplaus.models.plausibility_frame import PlausibilityFrame
from smcplaus.models.state import State

S0 = State(0)
S1 = State(1)
S2 = State(2)
S3 = State(3)
S4 = State(4)
S5 = State(5)
S6 = State(6)

STATE_TO_APPEARANCE_BAD = {
    S0: {S0, S2},
    S1: {S2, S3},
    S2: {S3},
    S3: set(),
}

STATE_TO_APPEARANCE_GOOD = {
    S4: {S4, S5, S6},
    S5: {S5, S6},
    S6: {S5, S6},
}


def test_check_valid_state_to_appearance_bad():
    with pytest.raises(IllDefinedStructureError):
        PlausibilityFrame._check_valid_state_to_appearance(domain={S0, S2}, state_to_appearance=STATE_TO_APPEARANCE_BAD)


def test_check_valid_state_to_appearance_good():
    _ = PlausibilityFrame._check_valid_state_to_appearance(
        domain={S0, S1, S2, S3, S4}, state_to_appearance=STATE_TO_APPEARANCE_BAD
    )


def test_check_reflexive_bad():
    with pytest.raises(IllDefinedStructureError):
        PlausibilityFrame._check_reflexive(STATE_TO_APPEARANCE_BAD)


def test_check_reflexive_good():
    _ = PlausibilityFrame._check_reflexive(STATE_TO_APPEARANCE_GOOD)


def test_check_transitive_bad():
    with pytest.raises(IllDefinedStructureError):
        PlausibilityFrame._check_transitive(STATE_TO_APPEARANCE_BAD)


def test_check_transitive_good():
    _ = PlausibilityFrame._check_transitive(STATE_TO_APPEARANCE_GOOD)


def test_check_total_bad():
    with pytest.raises(IllDefinedStructureError):
        PlausibilityFrame._check_total(STATE_TO_APPEARANCE_BAD)


def test_check_total_good():
    _ = PlausibilityFrame._check_total(STATE_TO_APPEARANCE_GOOD)


def test_make_reflexive():
    new_state_to_appearance = PlausibilityFrame._make_reflexive(STATE_TO_APPEARANCE_BAD)
    _ = PlausibilityFrame._check_reflexive(new_state_to_appearance)


def test_make_transitive():
    new_state_to_appearance = PlausibilityFrame._make_transitive(STATE_TO_APPEARANCE_BAD)
    _ = PlausibilityFrame._check_transitive(new_state_to_appearance)


def test_best_of():
    domain = {S4, S5, S6}
    frame = PlausibilityFrame(domain=domain, state_to_appearance=STATE_TO_APPEARANCE_GOOD)
    assert frame.best_of(domain) == {S5, S6}


def test_init_without_force_s4_throws_error():
    domain = {S0, S1, S2, S3}
    with pytest.raises(IllDefinedStructureError):
        _ = PlausibilityFrame(domain=domain, state_to_appearance=STATE_TO_APPEARANCE_BAD)


def test_init_with_force_s4_does_not_throw_error():
    domain = {S0, S1, S2, S3}
    _ = PlausibilityFrame(domain=domain, state_to_appearance=STATE_TO_APPEARANCE_BAD, force_s4=True)


FRAME_JSON_STR_INPUT = """
{
    "domain": ["s0", "s1", "s2", "s3"],
    "state_to_appearance": {
        "s0": ["s1"],
        "s1": ["s2"],
        "s2": ["s1", "s3"]
    }
}
"""


EXPECTED_FRAME = PlausibilityFrame(
    domain={S0, S1, S2, S3},
    state_to_appearance={
        S0: {S0, S1, S2, S3},
        S1: {S1, S2, S3},
        S2: {S1, S2, S3},
        S3: {S3},
    },
)


def test_from_json_without_force_s4():
    with pytest.raises(IllDefinedStructureError):
        _ = PlausibilityFrame.from_json(json_str=FRAME_JSON_STR_INPUT)


def test_from_json_with_force_s4():
    actual_frame = PlausibilityFrame.from_json(json_str=FRAME_JSON_STR_INPUT, force_s4=True)
    assert actual_frame == EXPECTED_FRAME


EXPECTED_FRAME_JSON_STR_OUTPUT = """
{
    "domain": [
        "s0",
        "s1",
        "s2",
        "s3"
    ],
    "state_to_appearance": {
        "s0": [
            "s0",
            "s1",
            "s2",
            "s3"
        ],
        "s1": [
            "s1",
            "s2",
            "s3"
        ],
        "s2": [
            "s1",
            "s2",
            "s3"
        ],
        "s3": [
            "s3"
        ]
    }
}
""".strip()


def test_to_json():
    assert EXPECTED_FRAME.to_json() == EXPECTED_FRAME_JSON_STR_OUTPUT


def test_get_clusters():
    frame = PlausibilityFrame(
        domain={
            S0,
            S1,
            S2,
            S3,
            S4,
            S5,
            S6,
        },
        state_to_appearance={
            S0: {S0, S1, S2, S3, S4, S5, S6},
            S1: {S1, S2, S3, S4, S5, S6},
            S2: {S1, S2, S3, S4, S5, S6},
            S3: {S1, S2, S3, S4, S5, S6},
            S4: {S4, S5, S6},
            S5: {S5, S6},
            S6: {S5, S6},
        },
    )
    expected = [
        {S0},
        {S1, S2, S3},
        {S4},
        {S5, S6},
    ]
    actual = frame.get_clusters()

    assert actual == expected

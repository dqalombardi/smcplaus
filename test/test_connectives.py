from smcplaus.language.connectives import CONNECTIVE_TO_ARITY, Connectives


def test_connectives_to_arity_exhaustive():
    connectives_set = set(Connectives)
    connectives_with_defined_arity = set(CONNECTIVE_TO_ARITY.keys())
    assert connectives_set == connectives_with_defined_arity

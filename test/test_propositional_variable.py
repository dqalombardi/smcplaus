import pytest

from smcplaus.exceptions import ConfigError, IllDefinedFormulaError
from smcplaus.language.propositional_variable import PropositionalVariable


def test_letter_not_ascii_lower_throws_error():
    with pytest.raises(IllDefinedFormulaError):
        _ = PropositionalVariable(letter="abc")
    with pytest.raises(IllDefinedFormulaError):
        _ = PropositionalVariable(letter="P")


def test_negative_index_throws_error():
    with pytest.raises(IllDefinedFormulaError):
        _ = PropositionalVariable(letter="p", index=-5)


def test_str():
    assert str(PropositionalVariable("p", 1)) == "p1"
    assert str(PropositionalVariable("q")) == "q"


def test_from_str_bad_pattern_throws_error():
    with pytest.raises(ConfigError):
        _ = PropositionalVariable.from_str("pp1")


def test_from_str_letter_s_throws_error():
    with pytest.raises(ConfigError):
        _ = PropositionalVariable.from_str("s")
    with pytest.raises(ConfigError):
        _ = PropositionalVariable.from_str("s1")


def test_from_str_no_index():
    expected = PropositionalVariable(letter="r", index=None)
    actual = PropositionalVariable.from_str("r")
    assert actual == expected

import pytest
from test_plausibility_frame import EXPECTED_FRAME

from smcplaus.exceptions import IllDefinedStructureError
from smcplaus.language.connectives import Connectives
from smcplaus.language.formula import Formula
from smcplaus.language.propositional_variable import PropositionalVariable
from smcplaus.models.plausibility_frame import PlausibilityFrame
from smcplaus.models.plausibility_model import PlausibilityModel, PointedPlausibilityModel
from smcplaus.models.state import State

S0 = State(0)
S1 = State(1)
S2 = State(2)
S3 = State(3)
S4 = State(4)

FRAME = PlausibilityFrame(
    domain={S0, S1, S2, S3},
    state_to_appearance={
        S0: {S0, S1, S2, S3},
        S1: {S1, S2, S3},
        S2: {S1, S2, S3},
        S3: {S3},
    },
)

P0 = PropositionalVariable("p", 0)
P1 = PropositionalVariable("p", 1)
P2 = PropositionalVariable("p", 2)
P3 = PropositionalVariable("p", 3)
P4 = PropositionalVariable("p", 4)
P5 = PropositionalVariable("p", 5)

F0 = Formula(node=P0, subformulas=None)
F1 = Formula(node=P1, subformulas=None)
F2 = Formula(node=P2, subformulas=None)
F3 = Formula(node=P3, subformulas=None)
F4 = Formula(node=P4, subformulas=None)
F5 = Formula(node=P5, subformulas=None)

STATE_TO_FACTS = {
    S0: {P2, P5},
    S1: {P0, P2, P3, P5},
    S2: {P1, P2, P3, P5},
    S3: {P0, P1, P2, P4},
}


MODEL = PlausibilityModel(frame=FRAME, state_to_facts=STATE_TO_FACTS)


def test_check_valid_state_to_facts_bad():
    with pytest.raises(IllDefinedStructureError):
        PlausibilityModel._check_valid_state_to_facts(
            frame=FRAME,
            state_to_facts={
                S0: {P2, P5},
                S4: {P0, P2, P3, P5},
                S2: {P1, P2, P3, P5},
                S3: {P0, P1, P2, P4},
            },
        )


def test_check_valid_state_to_facts_good():
    _ = PlausibilityModel._check_valid_state_to_facts(frame=FRAME, state_to_facts=STATE_TO_FACTS)


def test_truthset():
    assert MODEL.truthset(F0) == {S1, S3}


def test_update():
    frame_after_f0_update_expected = PlausibilityFrame(domain={S1, S3}, state_to_appearance={S1: {S1, S3}, S3: {S3}})
    model_after_f0_update_expected = PlausibilityModel(
        frame=frame_after_f0_update_expected,
        state_to_facts={
            state: facts for state, facts in STATE_TO_FACTS.items() if state in frame_after_f0_update_expected.domain
        },
    )
    assert MODEL.update(formula=F0) == model_after_f0_update_expected


def test_radical_upgrade():
    frame_after_f0_radical_upgrade_expected = PlausibilityFrame(
        domain={S0, S1, S2, S3},
        state_to_appearance={
            S0: {S0, S1, S2, S3},
            S2: {S1, S2, S3},
            S1: {S1, S3},
            S3: {S3},
        },
    )
    model_after_f0_radical_upgrade_expected = PlausibilityModel(
        frame=frame_after_f0_radical_upgrade_expected, state_to_facts=STATE_TO_FACTS
    )
    assert MODEL.radical_upgrade(formula=F0) == model_after_f0_radical_upgrade_expected


def test_conservative_upgrade():
    frame_after_conservative_upgrade_expected = PlausibilityFrame(
        domain={S0, S1, S2, S3},
        state_to_appearance={
            S0: {S0, S1, S2, S3},
            S1: {S1, S2},
            S2: {S1, S2},
            S3: {S1, S2, S3},
        },
    )
    model_after_conservative_upgrade_expected = PlausibilityModel(
        frame=frame_after_conservative_upgrade_expected, state_to_facts=STATE_TO_FACTS
    )
    assert MODEL.conservative_upgrade(formula=F5) == model_after_conservative_upgrade_expected


def test_satisfies_bot():
    formula = Formula(node=Connectives.BOT, subformulas=())  # F
    for state in MODEL.frame.domain:
        pointed_model = PointedPlausibilityModel(model=MODEL, point=state)
        assert not pointed_model.satisfies(formula=formula)


def test_satisfies_top():
    formula = Formula(node=Connectives.TOP, subformulas=())  # T
    for state in MODEL.frame.domain:
        pointed_model = PointedPlausibilityModel(model=MODEL, point=state)
        assert pointed_model.satisfies(formula=formula)


def test_satisfies_belief():
    formula = Formula(node=Connectives.BELIEF, subformulas=(F4,))  # B(p4)
    for state in MODEL.frame.domain:
        pointed_model = PointedPlausibilityModel(model=MODEL, point=state)
        assert pointed_model.satisfies(formula=formula)


def test_satisfies_box():
    subf = Formula(node=Connectives.OR, subformulas=(F3, F4))
    formula = Formula(node=Connectives.BOX, subformulas=(subf,))  # [](p3 || p4)
    pointed_model_s0 = PointedPlausibilityModel(model=MODEL, point=S0)
    assert not pointed_model_s0.satisfies(formula=formula)
    pointed_model_s1 = PointedPlausibilityModel(model=MODEL, point=S1)
    assert pointed_model_s1.satisfies(formula=formula)


def test_satisfies_diamond():
    formula = Formula(node=Connectives.DIAMOND, subformulas=(F5,))  # <>p5
    pointed_model_s2 = PointedPlausibilityModel(model=MODEL, point=S2)
    assert pointed_model_s2.satisfies(formula=formula)
    pointed_model_s3 = PointedPlausibilityModel(model=MODEL, point=S3)
    assert not pointed_model_s3.satisfies(formula=formula)


def test_satisfies_knowledge():
    formula = Formula(node=Connectives.KNOWLEDGE, subformulas=(F2,))  # K(p2)
    for state in MODEL.frame.domain:
        pointed_model = PointedPlausibilityModel(model=MODEL, point=state)
        assert pointed_model.satisfies(formula=formula)


def test_satisfies_not():
    formula = Formula(node=Connectives.NOT, subformulas=(F1,))  # ~(p1)
    pointed_model_s1 = PointedPlausibilityModel(model=MODEL, point=S1)
    assert pointed_model_s1.satisfies(formula=formula)
    pointed_model_s2 = PointedPlausibilityModel(model=MODEL, point=S2)
    assert not pointed_model_s2.satisfies(formula=formula)


def test_satisfies_strong_belief():
    formula = Formula(node=Connectives.STRONG_BELIEF, subformulas=(F4,))  # S(p4)
    for state in MODEL.frame.domain:
        pointed_model = PointedPlausibilityModel(model=MODEL, point=state)
        assert pointed_model.satisfies(formula=formula)


def test_satisfies_and():
    formula = Formula(node=Connectives.AND, subformulas=(F0, F1))  # p0 && p1
    pointed_model_s2 = PointedPlausibilityModel(model=MODEL, point=S2)
    assert not pointed_model_s2.satisfies(formula=formula)
    pointed_model_s3 = PointedPlausibilityModel(model=MODEL, point=S3)
    assert pointed_model_s3.satisfies(formula=formula)


def test_satisfies_conditional_belief():
    subf = Formula(node=Connectives.IMPLIES, subformulas=(F0, F1))
    formula = Formula(node=Connectives.CONDITIONAL_BELIEF, subformulas=(F5, subf))  # C(p5)(p0 || p1)
    for state in MODEL.frame.domain:
        pointed_model = PointedPlausibilityModel(model=MODEL, point=state)
        assert pointed_model.satisfies(formula=formula)


def test_satisfies_conservative_upgrade():
    subf0 = Formula(node=Connectives.DIAMOND, subformulas=(F5,))
    subf1 = Formula(node=Connectives.AND, subformulas=(F4, subf0))
    formula = Formula(node=Connectives.CONSERVATIVE_UPGRADE, subformulas=(F5, subf1))  # [^p5](p4 && <>p5)
    pointed_model_s0 = PointedPlausibilityModel(model=MODEL, point=S0)
    assert not pointed_model_s0.satisfies(formula=formula)
    pointed_model_s3 = PointedPlausibilityModel(model=MODEL, point=S3)
    assert pointed_model_s3.satisfies(formula=formula)


def test_satisfies_implies():
    formula = Formula(node=Connectives.IMPLIES, subformulas=(F0, F1))  # p0 -> p1
    pointed_model_s1 = PointedPlausibilityModel(model=MODEL, point=S1)
    assert not pointed_model_s1.satisfies(formula=formula)
    pointed_model_s3 = PointedPlausibilityModel(model=MODEL, point=S3)
    assert pointed_model_s3.satisfies(formula=formula)


def test_satisfies_or():
    formula = Formula(node=Connectives.OR, subformulas=(F0, F1))  # p0 || p1
    pointed_model_s0 = PointedPlausibilityModel(model=MODEL, point=S0)
    assert not pointed_model_s0.satisfies(formula=formula)
    pointed_model_s1 = PointedPlausibilityModel(model=MODEL, point=S1)
    assert pointed_model_s1.satisfies(formula=formula)


def test_satisfies_radical_upgrade():
    subf0 = Formula(node=Connectives.NOT, subformulas=(F0,))
    subf1 = Formula(node=Connectives.NOT, subformulas=(F1,))
    subf3 = Formula(node=Connectives.AND, subformulas=(subf0, subf1))
    subf4 = Formula(node=Connectives.BOX, subformulas=(F5,))
    formula = Formula(node=Connectives.RADICAL_UPGRADE, subformulas=(subf3, subf4))  # [$((~p0) && (~p1))]([]p5)
    pointed_model_s0 = PointedPlausibilityModel(model=MODEL, point=S0)
    assert pointed_model_s0.satisfies(formula=formula)
    pointed_model_s3 = PointedPlausibilityModel(model=MODEL, point=S3)
    assert not pointed_model_s3.satisfies(formula=formula)


def test_satisfies_update():
    subf = Formula(node=Connectives.BOX, subformulas=(F3,))
    formula = Formula(node=Connectives.UPDATE, subformulas=(F5, subf))  # [!p5]p3
    pointed_model_s0 = PointedPlausibilityModel(model=MODEL, point=S0)
    assert not pointed_model_s0.satisfies(formula=formula)
    pointed_model_s1 = PointedPlausibilityModel(model=MODEL, point=S1)
    assert pointed_model_s1.satisfies(formula=formula)


MODEL_JSON_STR_INPUT = """
{
    "frame": {
        "domain": ["s0", "s1", "s2", "s3"],
        "state_to_appearance": {
            "s0": ["s1"],
            "s1": ["s2"],
            "s2": ["s1", "s3"]
        }
    },
    "state_to_facts": {
        "s1": ["p0"],
        "s2": ["p1"],
        "s3": ["p0", "p1"]
    }
}
"""

EXPECTED_MODEL = PlausibilityModel(
    frame=EXPECTED_FRAME,
    state_to_facts={
        S0: set(),
        S1: {P0},
        S2: {P1},
        S3: {P0, P1},
    },
)


def test_from_json_plausibility_model():
    actual_model = PlausibilityModel.from_json(json_str=MODEL_JSON_STR_INPUT, force_s4=True)
    assert actual_model == EXPECTED_MODEL


POINTED_MODEL_JSON_STR_INPUT = """
{
    "model" : {
        "frame": {
            "domain": ["s0", "s1", "s2", "s3"],
            "state_to_appearance": {
                "s0": ["s1"],
                "s1": ["s2"],
                "s2": ["s1", "s3"]
            }
        },
        "state_to_facts": {
            "s1": ["p0"],
            "s2": ["p1"],
            "s3": ["p0", "p1"]
        }
    },
    "point" : "s2"
}
"""

EXPECTED_POINTED_MODEL = PointedPlausibilityModel(model=EXPECTED_MODEL, point=S2)


def test_from_json_pointed_plausibility_model():
    actual_pointed_model = PointedPlausibilityModel.from_json(json_str=POINTED_MODEL_JSON_STR_INPUT, force_s4=True)
    assert actual_pointed_model == EXPECTED_POINTED_MODEL


EXPECTED_MODEL_JSON_STR_OUTPUT = """
{
    "frame": {
        "domain": [
            "s0",
            "s1",
            "s2",
            "s3"
        ],
        "state_to_appearance": {
            "s0": [
                "s0",
                "s1",
                "s2",
                "s3"
            ],
            "s1": [
                "s1",
                "s2",
                "s3"
            ],
            "s2": [
                "s1",
                "s2",
                "s3"
            ],
            "s3": [
                "s3"
            ]
        }
    },
    "state_to_facts": {
        "s0": [],
        "s1": [
            "p0"
        ],
        "s2": [
            "p1"
        ],
        "s3": [
            "p0",
            "p1"
        ]
    }
}
""".strip()


def test_to_json_plausibility_model():
    assert EXPECTED_MODEL.to_json() == EXPECTED_MODEL_JSON_STR_OUTPUT


EXPECTED_POINTED_MODEL_JSON_STR_OUTPUT = """
{
    "model": {
        "frame": {
            "domain": [
                "s0",
                "s1",
                "s2",
                "s3"
            ],
            "state_to_appearance": {
                "s0": [
                    "s0",
                    "s1",
                    "s2",
                    "s3"
                ],
                "s1": [
                    "s1",
                    "s2",
                    "s3"
                ],
                "s2": [
                    "s1",
                    "s2",
                    "s3"
                ],
                "s3": [
                    "s3"
                ]
            }
        },
        "state_to_facts": {
            "s0": [],
            "s1": [
                "p0"
            ],
            "s2": [
                "p1"
            ],
            "s3": [
                "p0",
                "p1"
            ]
        }
    },
    "point": "s2"
}
""".strip()


def test_to_json_pointed_plausibility_model():
    assert EXPECTED_POINTED_MODEL.to_json() == EXPECTED_POINTED_MODEL_JSON_STR_OUTPUT

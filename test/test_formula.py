import pytest

from smcplaus.exceptions import IllDefinedFormulaError
from smcplaus.language.connectives import Connectives
from smcplaus.language.formula import Formula
from smcplaus.language.propositional_variable import PropositionalVariable

P0 = PropositionalVariable("p", 0)
P1 = PropositionalVariable("p", 1)
P2 = PropositionalVariable("p", 2)

F0 = Formula(node=P0, subformulas=None)
F1 = Formula(node=P1, subformulas=None)
F2 = Formula(node=P2, subformulas=None)


def test_init_with_prop_var_and_subformulas():
    with pytest.raises(IllDefinedFormulaError):
        _ = Formula(node=P2, subformulas=(F0, F1))


def test_init_with_unary_connective_and_none():
    with pytest.raises(IllDefinedFormulaError):
        _ = Formula(node=Connectives.BOX, subformulas=None)


def test_init_with_bad_node_type():
    with pytest.raises(TypeError):
        _ = Formula(node="p0", subformulas=None)  # type: ignore


def test_init_with_wrong_arity_throws_error():
    with pytest.raises(IllDefinedFormulaError):
        _ = Formula(node=Connectives.AND, subformulas=(F0,))


def test_init_with_correct_arity_does_not_throw_error():
    _ = Formula(node=Connectives.AND, subformulas=(F0, F1))

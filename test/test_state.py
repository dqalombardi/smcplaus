import pytest

from smcplaus.exceptions import ConfigError, IllDefinedStructureError
from smcplaus.models.state import State


def test_negative_index_throws_error():
    with pytest.raises(IllDefinedStructureError):
        _ = State(index=-5)


def test_str():
    assert str(State(index=5)) == "s5"


def test_from_str_bad_pattern_throws_error():
    with pytest.raises(ConfigError):
        _ = State.from_str("s")
    with pytest.raises(ConfigError):
        _ = State.from_str("0")
    with pytest.raises(ConfigError):
        _ = State.from_str("t0")

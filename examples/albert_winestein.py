"""Script to serve as an example of how to use smcplaus package."""

from pathlib import Path

from smcplaus.language.connectives import Connectives
from smcplaus.language.formula import Formula
from smcplaus.language.propositional_variable import PropositionalVariable
from smcplaus.models.plausibility_model import PlausibilityModel, PointedPlausibilityModel
from smcplaus.models.state import State

MODEL_JSON_FILEPATH = Path("./examples/albert_winestein.json")
MODEL_DIGRAPH_FILEPATH = Path("./examples/albert_winestein.dot")

_ALBERT_IS_DRUNK = Formula(node=PropositionalVariable("d"), subformulas=None)
_ALBERT_IS_A_GENIUS = Formula(node=PropositionalVariable("g"), subformulas=None)


def main() -> None:
    """Script entry point."""

    # Loading the model from a JSON file
    model_json_str = MODEL_JSON_FILEPATH.read_text(encoding="utf8")
    model = PlausibilityModel.from_json(model_json_str, force_s4=True)

    # Dumping the digraph to a DOT file
    model.dump_digraph(filepath=MODEL_DIGRAPH_FILEPATH)

    # Creating a pointed model
    pointed_model = PointedPlausibilityModel(model=model, point=State(0))

    # Albert does not know he's drunk
    form0 = Formula(  # ~Kd
        node=Connectives.NOT,
        subformulas=(
            Formula(
                node=Connectives.KNOWLEDGE,
                subformulas=(_ALBERT_IS_DRUNK,),
            ),
        ),
    )
    assert pointed_model.satisfies(form0)

    # Albert strongly believes he is not drunk
    form1 = Formula(  # S(~d)
        node=Connectives.STRONG_BELIEF,
        subformulas=(
            Formula(
                node=Connectives.NOT,
                subformulas=(_ALBERT_IS_DRUNK,),
            ),
        ),
    )
    assert pointed_model.satisfies(form1)

    # After a blood test showing he is drunk, Albert believes that he is not a genius
    form2 = Formula(  # [!d]B(~g)
        node=Connectives.UPDATE,
        subformulas=(
            _ALBERT_IS_DRUNK,
            Formula(
                node=Connectives.BELIEF,
                subformulas=(
                    Formula(
                        node=Connectives.NOT,
                        subformulas=(_ALBERT_IS_A_GENIUS,),
                    ),
                ),
            ),
        ),
    )
    assert pointed_model.satisfies(form2)

    # If Mary Curry tells Albert that he is drunk and Albert strongly trusts
    # her, then he will strongly believe that he is drunk
    form3 = Formula(  # [$d]Sd
        node=Connectives.RADICAL_UPGRADE,
        subformulas=(
            _ALBERT_IS_DRUNK,
            Formula(
                node=Connectives.STRONG_BELIEF,
                subformulas=(_ALBERT_IS_DRUNK,),
            ),
        ),
    )
    assert pointed_model.satisfies(form3)

    # If Mary Curry tells Albert that he is drunk and Albert weakly trusts
    # her, then he will believe that he is drunk
    form4 = Formula(  # [^d]Bd
        node=Connectives.CONSERVATIVE_UPGRADE,
        subformulas=(
            _ALBERT_IS_DRUNK,
            Formula(
                node=Connectives.BELIEF,
                subformulas=(_ALBERT_IS_DRUNK,),
            ),
        ),
    )
    assert pointed_model.satisfies(form4)

    return


if __name__ == "__main__":
    main()
